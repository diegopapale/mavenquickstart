package br.com.mavenquickstart;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class App 
{
	private HashSet<String> links;
	private List<List<String>> produtos;
	
	public App() {
        links = new HashSet<>();
        produtos = new ArrayList<>();
    }
    
    public static void main( String[] args )
    {
    	App bwc = new App();
        bwc.getProdutos();
        bwc.writeToFile("Produtos");
    }
    
    /*public void getPageLinks(String URL) {
        if (!links.contains(URL)) {
            try {
                Document document = Jsoup.connect(URL).get();
                Elements otherLinks = document.select("a[href^=\"http://www.americanas.com.br/\"]");

                for (Element page : otherLinks) {
                    if (links.add(URL)) {
                        //Remove the comment from the line below if you want to see it running on your editor
                        System.out.println(URL);
                    }
                    getPageLinks(page.attr("abs:href"));
                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }*/

    public void getProdutos() {
        links.forEach(x -> {
            Document document;
            try {
                document = Jsoup.connect(x).get();
                Elements articleLinks = document.select("h2 a[href^=\"http://www.americanas.com.br/\"]");
                for (Element article : articleLinks) {
                    if (article.text().matches("^.*?(Smartphone).*$")) {
                        
                        ArrayList<String> temporary = new ArrayList<>();
                        temporary.add(article.text());
                        temporary.add(article.attr("abs:href"));
                        produtos.add(temporary);
                    }
                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        });
    }

    public void writeToFile(String filename) {
        FileWriter writer;
        try {
            writer = new FileWriter(filename);
            produtos.forEach(a -> {
                try {
                    String temp = "- Title: " + a.get(0) + " (link: " + a.get(1) + ")\n";
                    //console
                    System.out.println(temp);
                    //save
                    writer.write(temp);
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            });
            writer.close();
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }
}
