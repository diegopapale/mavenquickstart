## Crawler Java

#### Procedimentos executados para criação e configuração do projeto:

* Download do Eclipse (Verifiquei em pesquisas que IDE mais utilizada pra Java)
* Download Maven
* Configuração JDK do Java (Verifiquei em leituras a necessidade de apontar o path da pasta dentro do Eclipse)
* Configuração do Maven (Verifiquei a necessidade de configurar o Maven dentro do Eclipse assim como o JDK)

### Desenvolvimento do projeto:

* Import (Visualizei em alguns posts a necessidade dos "imports" para uso de alguns pacotes semelhante ao "using" que utilizo em C#)

* List (Criei lista pra alocar os itens encontrados, lista => "produtos") 

* Método getProdutos:
- Responsável por Conectar-se a cada link e encontrar todos os produtos na página;
- Recupera apenas os produtos que contêm "Smartphone" no link, usei como exemplo;
- Adiciona os resultados em um array "produtos";

* Método writeToFile:
- Responsável por pegar a lista (array) de produtos e jogar em um arquivo na pasta raiz do projeto, aquivo => produtos;